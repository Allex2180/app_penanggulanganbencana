﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPL_Project_Enterprise
{
    public class Item : Entity
    {
        public virtual string Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Price { get; set; }
    }
}
