﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPL_Project_Enterprise
{
    public abstract class Entity
    {
        public abstract string Id { get; set; }
    }
    public abstract class Repository<T> where T : Entity, new()
    {
        protected List<T> list;
        protected DataVerbs Verb;
        protected string UserId = "pac_logmaster";
        protected string PassKey = "@jala12345!";

        public string DataProcess {
            get {
                Verb = DataVerbs.Get;
                var list = DataRetriving(Detail, (Model == null) ? Queryable(new T()) : Queryable(Model));
                return JsonCovert.serializeObject(list);
            }
            set {
                Verb = (DataVerbs)Convert.ToInt32(value);
                switch (Verb)
                {
                    case DataVerbs.Post:
                    case DataVerbs.Put:
                    case DataVerbs.Delete:
                    case DataVerbs.Alternate:
                        DataManipilating(Queryable, Model);
                        break;
                }
            }
        }
        public abstract T Model { get; set; }
        public abstract T Detail(dynamic result);
        public abstract string Query(T entity = null);

        public virtual string Generate(object type)
        {
            var state = new PglSQL_connection(UserId, PassKey);
            var list = "1";

            Verb = DataVerbs.Generate;
            state.OpenConnection = true;
            var query = Query((T)type);
            var result = state.CreateCommand(query).ExecuteReader();
            if (result.HasRows)
            {
                while (result.Read())
                {
                    list = (result.GetString(0)).ToString();
                }
            }
            state.CloseConnection = state.GetConnection;
            return JsonConvert.SearializeObject(new JObject { { "id", list } });
        }

        private void DataManipulating(Func<T, string> func, T entity)
        {
            var state = new PgSQL_Connection(UserId, PassKey);
            var query = func(entity);

            state.OpenConnection = true;
            state.CreateCommand(query).ExecuteNinQuery();
            state.CloseConnnection = state.GetConnection;
        }

        private List<T> DataRetrieving(Func<dynamic, T>func,string query)
        {
            var state = newPgSQL_Connection(UserId, PassKey);

            list = new List<T>();
            state.OpenConnection = true;
            var result = state.CreateComman(query).ExecuteReader();
            if (result.HasRows)
            {
                while (result.Read()) list.Add(func(result));
            }
            state.CloseConnection = state.GetConnection;
            return list;
        }
    }
}
