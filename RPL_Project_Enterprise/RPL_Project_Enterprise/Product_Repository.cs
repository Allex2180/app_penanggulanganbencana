﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _RPL_RepositoryConsole_Client

{
    public class ProductRepository : Repository<Product>
    {
        public override Product Model { get; set; }
        public override Product Detail(dynamic result)
        {
            var entity = new Product()
            {
                Id = result["Id"].ToString() as string,
                Description = result["Description"].ToString() as string,
            };
            return entity;
        }
        public override string Query(Product entity = null)
        {
            var query = string.Empty;
            switch (verb)
            {
                case DataVerbs.Get:
                    query = " ";
                    break;
                case DataVerbs.Post:
                    query = string.Format("", entity.id, entity.Description);
                    break;
                case DataVerbs.Put:
                    query = string.Format("", entity.Id, entity.Description);
                    break;
                case DataVerbs.Delete:
                    query = string.Format("", entity.Id);
                    break;
                case DataVerbs.Generate:
                    query = " ";
                    break;
            }
            return query;

        }


    }
}
